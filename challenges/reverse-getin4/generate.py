#/usr/bin/env python

solution = 'Kiwi'
ciphertext = ''
plaintext = ''

for c in solution:
    ciphertext = ciphertext + chr(ord(c) - 3)

print "Ciphertext: " + ciphertext

for c in ciphertext:
    plaintext = plaintext + chr(ord(c) + 3)

print "Plaintext: " + plaintext

