Title: Very Secret Base 3
Category: Reverse
Score: 200
Description:


Hi hacker! Try and crack this program `getin3`. 

- Download it from the CTF server (see below). 
- Go into the directory where you downloaded the file (remember? use `cd` command)
- Set it as executable.
- Finally, run it: `./getin3`
- Now you can try and guess the password... Good luck!
- Once you are short for ideas ;) you can try and do what is called *reversing*. This means you are going to study the executable with a special tool known as a disassembler and try to understand what it does.



## Listing strings in an executable

You should have learned how to do this in a previous `getin` challenge ;-)

Unfortunately, you won't spot the password that way in this case, because the string has been *encrypted*.

What is encryption? Basically, it is something that transforms a message into something nobody can understand... apart the persons who know how to and/or have a key to do so.


## Run Radare 2

Radare2 is a *disassembler* (but there are other disassemblers). 
It tries to make sense out of *executables*.

### Do you have radare2?

Open a Terminal. Then type `r2 -v`. If you get an answer such as "command not found", you need to [install Radare2](https://www.radare.org/r/down.html): **ask an adult to do that for you**.

### Disassemble the executable

Radare2's command is shortcut `r2`.

Run `r2 ./getin3`

```
$ r2 ./getin3
[0x00400660]>
```

The thing between brackets is the *address*. Radare understands a few commands. For example, try `aa` to tell it to "analyze all".

```
[x] Analyze all flags starting with sym. and entry0 (aa)
```

Now tell it to list all functions with command `afl`:

```
[0x00400660]> afl
0x00400598    3 26           sym._init
0x004005e0    2 16   -> 48   sym.imp.strlen
0x004005f0    2 16   -> 48   sym.imp.__stack_chk_fail
0x00400600    2 16   -> 48   sym.imp.printf
0x00400610    2 16   -> 48   sym.imp.strcspn
0x00400620    2 16   -> 48   sym.imp.__libc_start_main
0x00400630    2 16   -> 48   sym.imp.fgets
0x00400640    2 16   -> 48   sym.imp.strcmp
0x00400650    2 16   -> 48   loc.imp.__gmon_start__
0x00400660    1 41           entry0
0x00400690    4 50   -> 41   sym.deregister_tm_clones
0x004006d0    3 53           sym.register_tm_clones
0x00400710    3 28           sym.__do_global_dtors_aux
0x00400730    4 38   -> 35   entry1.init
0x00400756   10 266          main
0x00400860    4 101          sym.__libc_csu_init
0x004008d0    1 2            sym.__libc_csu_fini
0x004008d4    1 9            sym._fini
```

You can get help on a command using `?`. So for instance `af?` prints out help on all commands beginning with `af`.

### Disassemble main

The main of a program is its entry point. Kind of an important part, right?

We are going to disassemble it: `pdf @ main`

Ouch, you get plenty of ugly code :)

You can be expected to understand everything the first time. Let's try easy first.

This part prints the message "This is a very secret base".

```
           0x00400786      bfe8084000     mov edi, str.____This_is_a_very_secret_base____ ; 0x4008e8 ; "=== This is a very secret base ==="
|           0x0040078b      e840feffff     call sym.imp.puts           ; loc.imp
```

This part prints the message "Password: "

```
|           0x00400790      bf0b094000     mov edi, str.Password:      ; 0x40090b ; "Password: "
|           0x00400795      b800000000     mov eax, 0
|           0x0040079a      e861feffff     call sym.imp.printf         ; int printf(const char *format)
```

This part reads at most 25 characters you enter. Basically, `fgets` is a function to read a string. And `stdin` means you read from the input (what you type in).

```
|           0x0040079f      488b15ca0820.  mov rdx, qword [obj.stdin]  ; loc.imp. ; [0x601070:8]=0
|           0x004007a6      488d45c0       lea rax, [local_40h]
|           0x004007aa      be19000000     mov esi, 0x19               ; 25
|           0x004007af      4889c7         mov rdi, rax
|           0x004007b2      e879feffff     call sym.imp.fgets          ; char *fgets(char *s, int size, FILE *stream)
```

Now, we will simplify very much here, but this line adds 1 to a character of your input:

```
|     :||   0x004007e5      83c001         add eax, 1
```

We won't be reversing each line of the main, because you'd get bored before the end ;-) Just believe me, in this case, the program is:

1. Reading your input
2. Adding 1 to each character of your input
3. Comparing it with an *encrypted* password

### Adding 1 to a character?!

Computers understand characters as numbers. For example, `A` is  65, `B` is 66, `C`  is 67 etc. This transformation is called the ASCII table.

So, let's say you have character `A`. If we add 1, this is going to make 65+1=66. So this will correspond to `B`. Ok?

### Where is the encrypted password?

It is located at the beginning of the main in those `0x63667652`...

```
|           0x00400775      c745b0527666.  mov dword [local_50h], 0x63667652
|           0x0040077c      66c745b46664   mov word [local_4ch], 0x6466
```

Beware, 0x63 means *63 in base 16*. You might not understand what this means, but this 63 is not 63 but 99. And this corresponds actually to character `c`.

We are going to ask radare2 to convert this for us:

- Type `s 0x00400775+3`
- Then type `px 6`

```
[0x00400778]> px 6
- offset -   0 1  2 3  4 5  6 7  8 9  A B  C D  E F  0123456789ABCDEF
0x00400778  5276 6663 66c7                           Rvfcf.
```

This shows the character representation for (0x) 52 76 66 63 66: `Rvfcf`.

### Decrypt the password

So, `Rvfcf` is the encrypted password. We know that we add 1 to each character of the input. So, to decrypt, we do the opposite: we substract 1 to the encrypted password.

- Which letter is before R in the alphabet?
- Which letter is before v in the alphabet?
- Etc

You'll get the password! Try it, and it should work. Use the password to flag this challenge.


