#include <stdio.h>
#include <string.h>

int main(int argc, char **argv)
{
  char line[25];
  char pass[] = "Pingouin";

  printf("=== Ceci est une base super secrète ===\n");
  printf("Mot de passe: ");

  if (fgets(line, sizeof(line), stdin) != NULL)
  {
    line[strcspn(line, "\n")] = 0;
    if (strcmp(line, pass) == 0)
    {
      printf("\n ACCÈSS AUTORISÉ !!!\n");
      return 0;
    }
  }

  printf("Huh. Quelque chose ne s'est pas passé comme prévu !\n");
  return -1;
}
