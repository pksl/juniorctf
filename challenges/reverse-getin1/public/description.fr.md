Title: Base Secrète 1
Category: Reverse
Score: 50
Description:

Hello! Comment vas-tu? Que penses-tu de commencer quelques trucs *vraiment cools comme le reverse engineering*?

Essaye de cracker ce programme `getin`.

- Télécharge le fichier depuis le serveur (voir ci-dessous).
- Retrouve le fichier dans ton dossier de Téléchargements
- Transforme le en fichier exécutable: `chmod u+x getin`
- Finalement, pour le lancer sous Linux ou Mac:

```
$ ./getin  
=== Ceci est une base super secrète ===
Mot de passe:
```

Maintenant, tu peux essayer de deviner le mot de passe... Bonne chance !

## Manque d'idées?

Parfois, le mot de passe est juste écrit (caché) dans le programme lui-même. Que dirais-tu d'afficher tous les messages inclus dans l'exécutable ? Tu peux le faire sous Linux ou Mac avec une commande appelée "strings".

Essaye: `strings ./getin`

Tu vas peut-être voir plusieurs `strings`, plusieurs qui ne sont pas intéressantes pour le moment. Mais, certaines pourraient l'être, par exemple:

```
...
Pingouin
=== Ceci est une base super secrète ===
Mot de passe: 
 ACCÈS AUTORISÉ !!!
Huh. Quelque chose ne s'est pas passé comme prévu !
...
```

Le drapeau n'est pas très loin !
