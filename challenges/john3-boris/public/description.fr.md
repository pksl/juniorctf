Title: Boris casse du mot de passe
Category: Passwords
Score: 100
Description:

James, cet imbécile de Boris Grishenko a encore mis un mot de passe bizarre pour accéder au système de controle de Golden Eye. Les minutes comptent... il faut que tu trouves le mot de passe !

Nous avons que le mot de pass commence par le titre d'un James Bond (en anglais, en lettres minuscules, et sans aucun espace, par exemple, si c'était Golden Eye, ce serait `goldeneye`), suivi de **deux** chiffres.

## Attention

1. Cette épreuve est plus difficile que `john 2`. Je te conseille de faire d'abord john 1 et john 2.
2. Et devine ? Non, craquer les mots de passe des machines lorsqu'on n'a pas le droit, ce n'est cool, c'est interdit par la loi...


## Accéder au système de controle

Tu y accèdes par SSH:

- port `2222`
- nom d'utilisateur: `boris`
- l'adresse du serveur est `192.168.0.8`.

## Casser des mots de passe avec John

Il faut que tu emploies à peu près la meme technique qu'avec John 2. 
Sauf que cette fois-ci, je ne te fournis pas le fichier `john.conf`. Il faut que tu le fasses toi meme. Tu modifies le fichier que tu as fait dans john2 pour que cela puisse marcher dans ce cas ci. Ce n'est pas très compliqué, tu vas y arriver. L'Angleterre compte sur toi !

Ah ! Si john te dit quelque chose comme `Invalid rule in /etc/john/john.conf`, cela veut dire qu'il ne comprend pas tout à fait ton fichier de configuration. Tu as du faire une petite erreur. En particulier, pense à déplacer un peu les guillemets.

