Title: Launching Golden Eye
Category: Unix
Score: 50
Description:

For Boris Grishenko, life boils down to computers. And any information he needs to memorize are located on his computer.
Fortunately, 007 has just grabbed Boris's laptop and handed it over to the department of Q.

You, you work for Q and we want you to find important data in Boris's account. The information is hidden in files on the computer.
You get access to the Boris's laptop, but you're going to have to figure out where are the secret data...

## Logging on to Boris's laptop

Use SSH:

- port `2322`
- user: `boris`
- password: `computer`
- ordinateur: `IP ADDRESS`

## Files and directories

### Ok, I don't want to read all this, I want to find the solution!

Ahem. To find the solution, you're going to need to know how walk through directories and files.
As soon as you know:

- how to move from one directory to another
- read a file from a given location

you'll be able to solve this challenge.

The next paragraphs help you learn how to do this. If you already know this, happy you, ... solve the challenge!


### Tree

With Unix, all files are kept in a tree of directories. It's a bit like a real tree, with roots, a trunk and leafs. In Unix, the root and the trunk are the same and they correspond to a directory named `/`.

Then you have big branches. Those are the first directories like `/etc` or `/home`. Exactly, this means "inside etc directory from root".

Then you have smaller branches: those are subdirectories of your directory. Example `/home/susan`. This means "in directory susan, which is inside directory home starting from root". Ok?

### Path

There are two different ways to describe a path, i.e. the way to go to a given directory of file.

- **absolute path**. You explain how to go to that file or directory **from root**. Example: `/home/susan/blah.txt`. **An absolute path always begins with a /**.

- **relative path**. You explain how to go to that file/directory *from the location where you currently are*. Let's say you are in `/home/susan`. You want to go in `/etc`. You first need to go back in the directory that contains `susan`. That's `home`. Then, from `home`, you go back to the root. From root, you go to `/etc`. A relative path **never begins with /**.

With Unix, the directory you are currently in is called (shortcut): `.` (dot).
The directory that contains your own is your "father" and is shortcut: `..` (two dots).

Let's image you are in a directory named boris and have the following tree:

```
./boris/
|-------- Dockerfile
|-------- john.conf
|-------- public
|         |---- description.md
|-------- words
```

From directory `boris`, the relative path to `description.md` is: `./public/description.md`.
From directory `public`, the relative path to `john.conf` is : `../john.conf`

From directory `boris`, who is `./././././Dockerfile` ?
Think. `.` means the current directory. So, we're not changing to another directory and staying inside `boris`. So, this designes file `Dockerfile`. By the way, sometimes, instead of writing `./Dockerfile` (which is clear and precise) we write `Dockerfile` (which is less precise, but usually the context let you understand that's `./Dockerfile`).

What is the difference between `./john.conf`, `john.conf` and `/john.conf`. Beware...

- `./john.conf` means file `john.conf` in the current directory.
- `john.conf` means exactly the same. That's the same file.
- `/john.conf` starts with `/`. It's an absolute path. This is the file `john.conf` at the root directory. This is probably a very different file (unless you are currently located at root ;)


### Moving to another directory

To move to another directory, use command `cd` ("change directory") followed by the name of the directory you want to go to. You specify a path to that directory, either absolute or relative.

Example of absolute path: `cd /etc`
Example of relative path: `cd ./etc`.

Beware, because those two commands may get you in actually different directory. The second one gets you in subdirectory `etc` from where you are located. If you are in `/home/susan`, you'll go to `/home/susan/etc`.

### Read the content of a directory

Use command `ls` followed by the path (absolute or relative) of the directory you want to inspect.

Example: 

```
$ ls /usr
bin    include  lib32  local  share  x86_64-linux-gnu
games  lib      lib64  sbin   src
```

Command `ls` has many options. We won't detail all, but you'll file `ls -lh` useful. By the way, if you don't specify the directory, it means to list the current one.

Example: 

```
$ ls -lh
total 24K
drwxr-xr-x 3 axelle axelle 4,0K juil.  7 22:53 boris
-rwxr--r-- 1 axelle axelle  116 juil.  7 22:44 down.sh
drwxr-xr-x 3 axelle axelle 4,0K juil.  6 19:32 john1
drwxr-xr-x 3 axelle axelle 4,0K juil.  7 22:26 john2
drwxr-xr-x 3 axelle axelle 4,0K juil.  8 18:26 unix1
-rwxr--r-- 1 axelle axelle  176 juil.  7 22:44 up.sh
```

Note that:

- lines with begin with a lowercase `d` indicate this is a directory (name at the end on the right)
- the other lines are files (filename is at the end on the right)

So, boris, john1, john2 et unix1 are directories, while down.sh and up.sh are files.


### Read the contents of a file

You can open with `emacs`, but there's quicker. You can use a command named `cat`. Same, follow the command with an absolute or a relative path.

Example: 

```
$ cat ./host.conf 
# The "order" line is only used by old versions of the C library.
order hosts,bind
multi on
```

If the file is long, you'll prefer command `more` which displays the file one page at a time.

If you look into the content of a program (that's also called an executable), this is going to display many strange characters, because computers don't speak the same language as us !

If you try and read the content of a directory, `cat` will tell you this is not possible.

```
$ cat /etc
cat: /etc: Is a directory
```






