#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) {
  char line[25];
  char pass[] = "/CUVGT4CFCTG"; 
  int i;

  printf("=== I am the Loch Ness Monster ===\n");
  printf("Password: ");
  
  if (fgets(line, sizeof(line), stdin) !=NULL) {
    line[strcspn(line, "\n")]= 0;
    for (i=0; i<strlen(line); i++) {
      line[i] = line[i] - 30;
    }
    if (strcmp(line, pass) == 0 ) {
      printf("\nCongrats! Come in! ACCESS GRANTED. Flag is the password you found.\n");
      return 0;
    }
  }

  printf("Nope. Access denied!! Shall I eat you?\n");
  return -1;
}
