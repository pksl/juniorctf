Title: Hydra 2 - Frigo
Category: Passwords
Score: 100
Description: 

# Encore le frigo

Si tu ne l'as pas encore fait, je te conseille de résoudre le "Frigo de Pico" avant celui ci.

[Pico le Croco](http://picolecroco.free.fr) n'était pas content que tu lui piques son champagne. Il a décidé de changer de mot de passe pour quelque chose de vraiment plus compliqué. Vraiment.

Montre à Pico que tu y arrives toujours :)

**Indice: le mot de passe ne commence pas par un chiffre**

**Demande à un adulte d'installer pour toi [Hydra v9.0 ou +](https://github.com/vanhauser-thc/thc-hydra). Les versions en dessous ne marchent pas toujours bien pour cette énigme. **

## Ouvrir la Porte du frigo

On accède à la porte du frigo de Pico via SSH. Tu te rappelles comment faire ?

```
- Nom du frigo: ADRESSE IP
- Port: 2942
- identifiant: pico
```

Essaie de te connecter, pour voir, et tente quelques mots de passe.

## Mots de passe

Pico a retenu la lecon et n'a pas choisi un mot de passe figurant au palmares des 500 plus mauvais.

Télécharge plutot la liste [Ashley Madison](https://raw.githubusercontent.com/danielmiessler/SecLists/master/Passwords/Leaked-Databases/Ashley-Madison.txt). Il s'agit d'une liste de mots de passe assez fréquents qui a été trouvée par un attaquant en 2015. Cet attaquant avait récupéré les mots de passe d'un site en ligne qui s'appelle *Ashley Madison*.

## Purger la liste

La liste *Ashley Madison* est vraiment longue, et on te dit que le mot de passe ne commence pas par un chiffre.
Tu vas donc pouvoir enlever quelques mots de passe de la liste. Tu pourrais le faire à la main, mais cela risque d'etre un peu long.

Ouvre un terminal, et lance *exactement* la commande suivante:

`grep -v -E "^[0-9]+" Ashley-Madison.txt > ashley-purge.txt`

Cette commande permet d'enlever tous les mots de passe qui commencent par des chiffres.
Si tu n'y arrives pas, télécharge la liste `ashley-purge.txt` jointe à cette énigme.

## Ca risque d'etre long

Ca va etre long. Très long. Quelques heures probablement. Arme toi de patience, et laisse ton ordinateur travailler tout seul, pendant que tu fais d'autres énigmes par exemple !

Déja, ajoute l'option `-F` pour t'arreter dès qu'hydra trouve un mot de passe qui convient. Ok?

Ensuite, tu auras sans doute besoin d'arreter ton ordinateur entre temps.
Comment reprendre après ?

La bonne nouvelle: **tout n'est pas perdu**.

Dans ce cas là, **conserve bien le fichier hydra.restore** qui se trouve dans ton répertoire.

Plus tard, quand tu voudras **relancer la recherche là où tu t'es arreté**, lance:

```
$ hydra -R
```

## Ca ne marche pas, je ne trouve rien

1. Essaie de te connecter toi meme au frigo.
2. Verifie la ligne de commande d'hydra: est-ce que par exemple le fichier de mots de passe est au bon endroit
3. Verifie que ton fichier de mots de passe n'est pas vide...
4. Verifie que tu utilises Hydra 9.0 ou plus récent.

## Rappels pour la vraie vie

- Quand tu as soif, c'est mieux de prendre de l'eau ;)
- C'est **interdit** d'accéder à des machines si on ne t'y a pas autorisé. Non vraiment (amende, prison et tout).
- Attaquer un site ce n'est pas "cool" mais bete et irresponsable. Ce n'est pas parce que quelqu'un utilise des mots de passe pourris que tu as le droit de les utiliser à sa place. 

