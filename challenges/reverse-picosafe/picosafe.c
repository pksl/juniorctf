#include <stdio.h>
#include <string.h>
#include <stdlib.h>

 /* 2007 + 1234 */
#define ENCODED_PIN 3241

int check_pin(int pin) {
  if (pin + 1234 == ENCODED_PIN) 
    return 1; 
  return 0;
}


/* this function will not work correctly for PINs less that 4 digits */
int ask_pin(void) {
  int pin_value = 0;
  printf("PIN code: ");
  scanf("%d", &pin_value);
  return pin_value;
}


void draw_thesafe(void) {
  printf("===== This is Pico s Safe ====== \n");
  printf("|                              |\n");
  printf("|                              |\n");
  printf("|         ---------            |\n");
  printf("|        *  . .  . *           |\n");
  printf("|       | .   |   . |          |\n");
  printf("|       | .   |   . |          |\n");
  printf("|       | .       . |          |\n");
  printf("|        *  . .  . *           |\n");
  printf("|         ---------            |\n");
  printf("|                              |\n");
  printf("|------------------------------|\n");
}

void open_thesafe(void) {
  int check = 0;
  int attempts = 3;
  int i;

  for (i=0; i<attempts; i++) {
    int pin = ask_pin();
    check = check_pin(pin);
    if (check)
      break;
  }
  if (check) {
    printf("Well done! Grab the $$$ ! The flag is Dollars_xxxx where you replace xxxx with the PIN code you found\n");
  } else {
    printf("Arg! You triggered an alarm! The Police will soon arrive...\n");
  }
} 


int main(int argc, char **argv) {
  open_thesafe();
  return 0;
}
