#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) {
  char line[25];
  char pass[] = "CrocodilesAreBig";

  printf("=== This is a very secret base ===\n");
  printf("Password: ");
  
  if (fgets(line, sizeof(line), stdin) !=NULL) {
    line[strcspn(line, "\n")]= 0;
    if (strcmp(line, pass) == 0 ) {
      printf("\n ACCESS GRANTED !!!\n");
      printf("Use the password as the flag for the challenge\n");
      return 0;
    }
  }

  printf("Access denied kid!!\n");
  return -1;
}
